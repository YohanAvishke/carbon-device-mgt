/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.wso2.carbon.device.application.mgt.core.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.wso2.carbon.context.PrivilegedCarbonContext;
import org.wso2.carbon.device.application.mgt.common.ExecutionStatus;
import org.wso2.carbon.device.application.mgt.common.SubscriptionType;
import org.wso2.carbon.device.application.mgt.common.dto.ApplicationDTO;

import org.wso2.carbon.device.application.mgt.common.dto.ApplicationReleaseDTO;
import org.wso2.carbon.device.application.mgt.common.dto.DeviceSubscriptionDTO;
import org.wso2.carbon.device.application.mgt.common.dto.ReviewDTO;
import org.wso2.carbon.device.application.mgt.common.dto.ScheduledSubscriptionDTO;
import org.wso2.carbon.device.application.mgt.common.services.ApplicationManager;
import org.wso2.carbon.device.application.mgt.common.services.ApplicationStorageManager;
import org.wso2.carbon.device.application.mgt.common.services.SubscriptionManager;
import org.wso2.carbon.device.application.mgt.core.exception.UnexpectedServerErrorException;
import org.wso2.carbon.device.mgt.common.DeviceIdentifier;
import org.wso2.carbon.device.mgt.core.service.DeviceManagementProviderService;
import org.wso2.carbon.device.mgt.core.service.DeviceManagementProviderServiceImpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * This class is responsible for handling the utils of the Application Management DAO.
 */
public class DAOUtil {

    private static final Log log = LogFactory.getLog(DAOUtil.class);

    /**
     * To create application object from the result set retrieved from the Database.
     *
     * @param rs ResultSet
     * @return List of Applications that is retrieved from the Database.
     * @throws SQLException  SQL Exception
     * @throws JSONException JSONException.
     */
    public static List<ApplicationDTO> loadApplications(ResultSet rs) throws SQLException, JSONException {

        List<ApplicationDTO> applications = new ArrayList<>();
        ApplicationDTO application = null;
        int applicationId = -1;
        boolean hasNext = rs.next();

        while (hasNext) {
            if (applicationId != rs.getInt("APP_ID")) {
                if (application != null) {
                    applications.add(application);
                }
                application = new ApplicationDTO();
                application.setApplicationReleaseDTOs(new ArrayList<>());
                applicationId = rs.getInt("APP_ID");
                application.setId(applicationId);
                application.setName(rs.getString("APP_NAME"));
                application.setDescription(rs.getString("APP_DESCRIPTION"));
                application.setType(rs.getString("APP_TYPE"));
                application.setSubType(rs.getString("APP_SUB_TYPE"));
                application.setPaymentCurrency(rs.getString("APP_CURRENCY"));
                application.setStatus(rs.getString("APP_STATUS"));
                application.setAppRating(rs.getDouble("APP_RATING"));
                application.setDeviceTypeId(rs.getInt("APP_DEVICE_TYPE_ID"));
                application.setPackageName(rs.getString("PACKAGE_NAME"));
                application.getApplicationReleaseDTOs().add(constructAppReleaseDTO(rs));
            } else {
                if (application != null && application.getApplicationReleaseDTOs() != null) {
                    application.getApplicationReleaseDTOs().add(constructAppReleaseDTO(rs));
                }
            }
            hasNext = rs.next();
            if (!hasNext) {
                applications.add(application);
            }
        }
        return applications;
    }

    /**
     * To create list of device subscription objects from the result set retrieved from the Database.
     *
     * @param rs ResultSet
     * @return List of device subscriptions that is retrieved from the Database.
     * @throws SQLException  SQL Exception
     * @throws JSONException JSONException.
     */
    public static List<DeviceSubscriptionDTO> loadDeviceSubscriptions(ResultSet rs) throws SQLException {
        List<DeviceSubscriptionDTO> deviceSubscriptionDTOS = new ArrayList<>();
        while (rs.next()) {
            deviceSubscriptionDTOS.add(constructDeviceSubscriptionDTO(rs));
        }
        return deviceSubscriptionDTOS;
    }

    public static DeviceSubscriptionDTO constructDeviceSubscriptionDTO(ResultSet rs ) throws SQLException {
        DeviceSubscriptionDTO deviceSubscriptionDTO = new DeviceSubscriptionDTO();
        deviceSubscriptionDTO.setId(rs.getInt("ID"));
        deviceSubscriptionDTO.setSubscribedBy(rs.getString("SUBSCRIBED_BY"));
        deviceSubscriptionDTO.setSubscribedTimestamp(rs.getTimestamp("SUBSCRIBED_AT"));
        deviceSubscriptionDTO.setUnsubscribed(rs.getBoolean("IS_UNSUBSCRIBED"));
        deviceSubscriptionDTO.setUnsubscribedBy(rs.getString("UNSUBSCRIBED_BY"));
        deviceSubscriptionDTO.setUnsubscribedTimestamp(rs.getTimestamp("UNSUBSCRIBED_AT"));
        deviceSubscriptionDTO.setActionTriggeredFrom(rs.getString("ACTION_TRIGGERED_FROM"));
        deviceSubscriptionDTO.setDeviceId(rs.getInt("DEVICE_ID"));
        deviceSubscriptionDTO.setStatus(rs.getString("STATUS"));
        return  deviceSubscriptionDTO;
    }

    /**
     * Populates {@link ApplicationReleaseDTO} object with the result obtained from the database.
     *
     * @param rs {@link ResultSet} from obtained from the database
     * @return {@link ApplicationReleaseDTO} object populated with the data
     * @throws SQLException If unable to populate {@link ApplicationReleaseDTO} object with the data
     */
    public static ApplicationReleaseDTO constructAppReleaseDTO(ResultSet rs) throws SQLException {
        ApplicationReleaseDTO appRelease = new ApplicationReleaseDTO();
        appRelease.setId(rs.getInt("RELEASE_ID"));
        appRelease.setDescription(rs.getString("RELEASE_DESCRIPTION"));
        appRelease.setUuid(rs.getString("RELEASE_UUID"));
        appRelease.setReleaseType(rs.getString("RELEASE_TYPE"));
        appRelease.setVersion(rs.getString("RELEASE_VERSION"));
        appRelease.setInstallerName(rs.getString("AP_RELEASE_STORED_LOC"));
        appRelease.setIconName(rs.getString("AP_RELEASE_ICON_LOC"));
        appRelease.setBannerName(rs.getString("AP_RELEASE_BANNER_LOC"));
        appRelease.setScreenshotName1(rs.getString("AP_RELEASE_SC1"));
        appRelease.setScreenshotName2(rs.getString("AP_RELEASE_SC2"));
        appRelease.setScreenshotName3(rs.getString("AP_RELEASE_SC3"));
        appRelease.setAppHashValue(rs.getString("RELEASE_HASH_VALUE"));
        appRelease.setPrice(rs.getDouble("RELEASE_PRICE"));
        appRelease.setMetaData(rs.getString("RELEASE_META_INFO"));
        appRelease.setPackageName(rs.getString("PACKAGE_NAME"));
        appRelease.setSupportedOsVersions(rs.getString("RELEASE_SUP_OS_VERSIONS"));
        appRelease.setRating(rs.getDouble("RELEASE_RATING"));
        appRelease.setCurrentState(rs.getString("RELEASE_CURRENT_STATE"));
        appRelease.setRatedUsers(rs.getInt("RATED_USER_COUNT"));
        return appRelease;
    }


    /**
     * To create application object from the result set retrieved from the Database.
     *
     * @param rs ResultSet
     * @return ApplicationDTO that is retrieved from the Database.
     * @throws SQLException  SQL Exception
     * @throws JSONException JSONException.
     */
    public static ApplicationDTO loadApplication(ResultSet rs)
            throws SQLException, JSONException, UnexpectedServerErrorException {
        List<ApplicationDTO> applicationDTOs = loadApplications(rs);
        if (applicationDTOs.isEmpty()) {
            return null;
        }
        if (applicationDTOs.size() > 1) {
            String msg = "Internal server error. Found more than one application for requested application ID";
            log.error(msg);
            throw new UnexpectedServerErrorException(msg);
        }
        return applicationDTOs.get(0);
    }

    /**
     * Populates {@link ApplicationReleaseDTO} object with the result obtained from the database.
     *
     * @param resultSet {@link ResultSet} from obtained from the database
     * @return {@link ApplicationReleaseDTO} object populated with the data
     * @throws SQLException If unable to populate {@link ApplicationReleaseDTO} object with the data
     */
    public static ApplicationReleaseDTO loadApplicationRelease(ResultSet resultSet) throws SQLException {
        ApplicationReleaseDTO applicationRelease = new ApplicationReleaseDTO();
        applicationRelease.setId(resultSet.getInt("RELEASE_ID"));
        applicationRelease.setVersion(resultSet.getString("RELEASE_VERSION"));
        applicationRelease.setUuid(resultSet.getString("UUID"));
        applicationRelease.setReleaseType(resultSet.getString("RELEASE_TYPE"));
        applicationRelease.setPackageName(resultSet.getString("PACKAGE_NAME"));
        applicationRelease.setPrice(resultSet.getDouble("APP_PRICE"));
        applicationRelease.setInstallerName(resultSet.getString("STORED_LOCATION"));
        applicationRelease.setBannerName(resultSet.getString("BANNER_LOCATION"));
        applicationRelease.setIconName(resultSet.getString("ICON_LOCATION"));
        applicationRelease.setScreenshotName1(resultSet.getString("SCREEN_SHOT_1"));
        applicationRelease.setScreenshotName2(resultSet.getString("SCREEN_SHOT_2"));
        applicationRelease.setScreenshotName3(resultSet.getString("SCREEN_SHOT_3"));
        applicationRelease.setAppHashValue(resultSet.getString("HASH_VALUE"));
        applicationRelease.setIsSharedWithAllTenants(resultSet.getBoolean("SHARED"));
        applicationRelease.setMetaData(resultSet.getString("APP_META_INFO"));
        applicationRelease.setRating(resultSet.getDouble("RATING"));
        return applicationRelease;
    }

    public static ReviewDTO loadReview(ResultSet rs) throws SQLException, UnexpectedServerErrorException {
        List<ReviewDTO> reviewDTOs = loadReviews(rs);
        if (reviewDTOs.isEmpty()) {
            return null;
        }
        if (reviewDTOs.size() > 1) {
            String msg = "Internal server error. Found more than one review for requested review ID";
            log.error(msg);
            throw new UnexpectedServerErrorException(msg);
        }
        return reviewDTOs.get(0);
    }

    public static List<ReviewDTO> loadReviews (ResultSet rs) throws SQLException {
        List<ReviewDTO> reviewDTOs = new ArrayList<>();
        while (rs.next()) {
            ReviewDTO reviewDTO = new ReviewDTO();
            reviewDTO.setId(rs.getInt("ID"));
            reviewDTO.setContent(rs.getString("COMMENT"));
            reviewDTO.setCreatedAt(rs.getTimestamp("CREATED_AT"));
            reviewDTO.setModifiedAt(rs.getTimestamp("MODIFIED_AT"));
            reviewDTO.setRootParentId(rs.getInt("ROOT_PARENT_ID"));
            reviewDTO.setImmediateParentId(rs.getInt("IMMEDIATE_PARENT_ID"));
            reviewDTO.setUsername(rs.getString("USERNAME"));
            reviewDTO.setRating(rs.getInt("RATING"));
            reviewDTO.setReleaseUuid(rs.getString("UUID"));
            reviewDTO.setReleaseVersion(rs.getString("VERSION"));
            reviewDTOs.add(reviewDTO);
        }
        return reviewDTOs;
    }

    public  static ScheduledSubscriptionDTO loadScheduledSubscription(ResultSet rs)
            throws SQLException, UnexpectedServerErrorException {
        List<ScheduledSubscriptionDTO> subscriptionDTOs = loadScheduledSubscriptions(rs);

        if (subscriptionDTOs.isEmpty()) {
            return null;
        }
        if (subscriptionDTOs.size() > 1) {
            String msg = "Internal server error. Found more than one subscription for requested pending subscription";
            log.error(msg);
            throw new UnexpectedServerErrorException(msg);
        }
        return subscriptionDTOs.get(0);
    }

    public static List<ScheduledSubscriptionDTO> loadScheduledSubscriptions(ResultSet rs) throws SQLException {
        List<ScheduledSubscriptionDTO> subscriptionDTOS = new ArrayList<>();
        while (rs.next()) {
            ScheduledSubscriptionDTO subscription = new ScheduledSubscriptionDTO();
            subscription.setId(rs.getInt("ID"));
            subscription.setTaskName(rs.getString("TASK_NAME"));
            subscription.setApplicationUUID(rs.getString("APPLICATION_UUID"));

            if (subscription.getTaskName().startsWith(SubscriptionType.DEVICE.toString())) {
                List<DeviceIdentifier> deviceIdentifiers = new Gson().fromJson(rs.getString("SUBSCRIBER_LIST"),
                        new TypeToken<List<DeviceIdentifier>>() {
                        }.getType());
                subscription.setSubscriberList(deviceIdentifiers);
            } else {
                List<String> subscriberList = Pattern.compile(",").splitAsStream(rs.getString("SUBSCRIBER_LIST"))
                        .collect(Collectors.toList());
                subscription.setSubscriberList(subscriberList);
            }

            subscription.setStatus(ExecutionStatus.valueOf(rs.getString("STATUS")));
            subscription.setScheduledAt(rs.getTimestamp("SCHEDULED_AT").toLocalDateTime());
            subscription.setScheduledBy(rs.getString("SCHEDULED_BY"));
            subscription.setDeleted(rs.getBoolean("DELETED"));
            subscriptionDTOS.add(subscription);
        }
        return subscriptionDTOS;
    }

    /**
     * Cleans up the statement and resultset after executing the query
     *
     * @param stmt Statement executed.
     * @param rs   Resultset retrived.
     */
    public static void cleanupResources(PreparedStatement stmt, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.warn("Error occurred while closing result set", e);
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                log.warn("Error occurred while closing prepared statement", e);
            }
        }
    }

    private static ApplicationManager applicationManager;
    private static ApplicationStorageManager applicationStorageManager;
    private static SubscriptionManager subscriptionManager;

    public static ApplicationManager getApplicationManager() {
        if (applicationManager == null) {
            synchronized (DAOUtil.class) {
                if (applicationManager == null) {
                    PrivilegedCarbonContext ctx = PrivilegedCarbonContext.getThreadLocalCarbonContext();
                    applicationManager =
                            (ApplicationManager) ctx.getOSGiService(ApplicationManager.class, null);
                    if (applicationManager == null) {
                        String msg = "ApplicationDTO Manager service has not initialized.";
                        log.error(msg);
                        throw new IllegalStateException(msg);
                    }
                }
            }
        }
        return applicationManager;
    }

    /**
     * To get the ApplicationDTO Storage Manager from the osgi context.
     * @return ApplicationStoreManager instance in the current osgi context.
     */
    public static ApplicationStorageManager getApplicationStorageManager() {

        try {
            if (applicationStorageManager == null) {
                synchronized (DAOUtil.class) {
                    if (applicationStorageManager == null) {
                        applicationStorageManager = ApplicationManagementUtil
                                .getApplicationStorageManagerInstance();
                        if (applicationStorageManager == null) {
                            String msg = "ApplicationDTO Storage Manager service has not initialized.";
                            log.error(msg);
                            throw new IllegalStateException(msg);
                        }
                    }
                }
            }
        } catch (Exception e) {
            String msg = "Error occurred while getting the application store manager";
            log.error(msg);
            throw new IllegalStateException(msg);
        }
        return applicationStorageManager;
    }


    /**
     * To get the Subscription Manager from the osgi context.
     * @return SubscriptionManager instance in the current osgi context.
     */
    public static SubscriptionManager getSubscriptionManager() {
        if (subscriptionManager == null) {
            synchronized (DAOUtil.class) {
                if (subscriptionManager == null) {
                    PrivilegedCarbonContext ctx = PrivilegedCarbonContext.getThreadLocalCarbonContext();
                    subscriptionManager =
                            (SubscriptionManager) ctx.getOSGiService(SubscriptionManager.class, null);
                    if (subscriptionManager == null) {
                        String msg = "Subscription Manager service has not initialized.";
                        log.error(msg);
                        throw new IllegalStateException(msg);
                    }
                }
            }
        }

        return subscriptionManager;
    }

    public static DeviceManagementProviderService getDeviceManagementService() {
//        PrivilegedCarbonContext ctx = PrivilegedCarbonContext.getThreadLocalCarbonContext();
//        DeviceManagementProviderService deviceManagementProviderService =
//                (DeviceManagementProviderService) ctx.getOSGiService(DeviceManagementProviderService.class, null);
//        if (deviceManagementProviderService == null) {
//            String msg = "DeviceImpl Management provider service has not initialized.";
//            log.error(msg);
//            throw new IllegalStateException(msg);
//        }
       // return deviceManagementProviderService;

        return  new DeviceManagementProviderServiceImpl();
    }
}
